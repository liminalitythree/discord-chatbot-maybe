import gpt_2_simple as gpt2
import discord
from collections import deque
import threading
import queue
import asyncio

MSG_CHANNEL_ID = 712568607883919383

qin = queue.Queue()
qout = queue.SimpleQueue()

def worker():
    sess = gpt2.start_tf_sess()
    gpt2.load_gpt2(sess)

    while True:
        item = qin.get()
        print(f'Working on {item}')
        msg = gpt2.generate(sess,
            length=100,
            temperature=0.7,
            prefix=item,
            nsamples=1,
            return_as_list=True,
            include_prefix=False,
            truncate='<|endoftext|>'
            )[0]
        
        msg = msg.replace('<|startoftext|>', '')
        msg = msg.replace('<|br|>', '\n')
        msg = msg.replace('<|endoftext|>', '')
        
        print(f'Finished {item}')
        qout.put(msg, True, 15)
        qin.task_done()

threading.Thread(target=worker, daemon=True).start()


def replace_with_tokens(word):
    if word.startswith('<@'):
        return '<|Mention|>'
    if word.startswith('http'):
        return '<|Url|>'
    if word.startswith('www.'):
        return '<|Url|>'

    return word

def history_to_txt(inhistory):
    # make sure history is not too long
    history = deque(inhistory)
    while len(history) > 50:
        history.popleft()

    finalarr = []
    linearr = []
    curid = ''
    for msg in history:
        if curid != msg.author.id:
            if curid != '':
                txt = '<|startoftext|>'
                for l in linearr:
                    if txt != '<|startoftext|>':
                        txt = txt + '<|br|>'
                    txt = txt + l
                txt = txt + '<|endoftext|>'
                finalarr.append(txt)
                linearr = []
            curid = msg.author.id
        
        msg = msg.content
        msg = msg.split(' ')
        line = list(map(replace_with_tokens, msg))
        line = ' '.join(line)
        linearr.append(line)
    
    txt = '<|startoftext|>'
    for l in linearr:
        if txt != '<|startoftext|>':
            txt = txt + '<|br|>'
        txt = txt + l
    txt = txt + '<|endoftext|>'
    
    finalarr.append(txt)
        

    out = '\n'.join(finalarr)
    # remove non utf8 characters
    out = bytes(out, 'utf-8').decode('utf-8', 'ignore')
    return out





class LillyBot(discord.Client):
    history = []

    async def on_ready(self):
        print('Logged on as', self.user)

    async def on_message(self, message):
        # don't respond to ourselves
        if message.author == self.user:
            return
        
        if message.content == '.reply':
            text = history_to_txt(self.history)
            self.history = []
            qin.put_nowait(text)


        self.history.append(message)


async def post_messages():
    while True:
        await asyncio.sleep(1)
        msg = ''
        empty = False
        try:
            msg = qout.get_nowait()
        except queue.Empty:
            empty = True
        
        if empty != True:
            channel = client.get_channel(MSG_CHANNEL_ID)
            await channel.send(msg)
    

client = LillyBot()

client.loop.create_task(post_messages())

client.run('NzExNTQwNzcwNzM3MjI1NzQ5.XsTdeQ.Xq1Am6IJSkuRjwyOy0wUbiDGTZk')
